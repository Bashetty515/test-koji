**First of all thank you so much KOJI for giving this opportunity to write a techinacal exam on Docker, which is my favoutire tool**

I will explain all the steps what I did from scratch.

**Composer** => Composer is an application-level package manager for the PHP programming language that provides a standard format for managing dependencies of PHP software and required libraries.

**bedrock** => Bedrock is an open source project made by Roots that you use as base for WordPress projects. To install bedrock I must need composer

First step is, I installed composer manually on my local system and I created a new project using composer with below command

`composer create-project roots/bedrock`

This above command created a new directory and the name of the directory called bedrock, but when I tried to create a new directory I faced error regarding **php -simplexml files** and I instll with below command.

`sudo apt install php-simplexml`

2nd step, Changed directory to bedrock folder and give command 

`composer update`

This command updated all dependencies whcih needed for wordpress

Next, I updated the environment variable in `.env` file with below modifications

DB_NAME=wordpress 

DB_USER=root

DB_HOST=mysql:3306

WP_HOME=http://localhost

DB_PASSWORD=password

WP_SITEURL=${WP_HOME}/wp

WP_ENV=development


**Sage** => Sage is one of the WordPress starter themes that it provides for front-end and back-end development, and it comes with many sophisticated modern features.

Make sure all dependencies have been installed before moving on:
WordPress >= 4.7

PHP >= 7.1.3 (with php-mbstring enabled)

Composer

Node.js >= 8.0.0

Yarn

After installing all dependencies on localhost, I Installed Sage using Composer from WordPress themes directory (I replaced theme name called KOJI):

so, the command is `composer create-project roots/sage KOJI`, this command must be excute from either **app/themes/ or wp-content/themes/**

Whenever I run this command, I had the option to define theme meta information **(name, URI, description, version, author) and choose a CSS framework (Bootstrap).**

From the command line on your host machine  navigate to the theme directory called First-theme then run the command yarn:

This **yarn command install all the dependencies** which I needed to build the process

Now, for Browsersync configuration, I updated devUrl at the bottom of resources/assets/config.json to reflect local development hostname.

For example, if your local development URL is https://First-theme.test you would update the file to read: 

`"devUrl": "https://First-theme.test"`

**Now the docker-compose file came into the game :)**


I wroted 5 docker services in docker-compose file, which I needed to run the wordpress with bedrock and sage, here goes the list of serices,

1. Nginix server

1. MySQL Database 

1. Wordpress service

1. PHP admin to view the databse
 
In Docker-compose file, before defining the services I used a **version tag** to define the Compose file format, which is 3.6. There are other file formats — 1, 2, 2.x, and 3.x. 

To make the setup process quick and easy, I used the pre-built official image of **Mariadb, phpmyadmin/phpmyadmin, nginix, wordpress_php7.2-fpm,composer** for the image tag.

I used the container_name for all the services for more understandable, also used always for the **restart tag** the container always restarts. It can save time. For example, you don’t have to start the container every time you reboot your machine manually. It restarts the container when either the Docker daemon restarts or the container itself is manually restarted.

networks: This specifies that our application service will join the wpsite-network network, which we will define at the bottom of the each docker file.

**Nginix**

I am very much familiar with `nginx server`, so I gone through this config file and I wrote seperate vhost config file called `vhost.conf`, nginx have default config when we have with docker and I moddified the vhost file as per our requirements. I used **nginix for web browser**

The image keyword lets docker know what image we want to use to build our container: in this case I used nginx:latest which will provide us with the nginix web server. Need another nginx version? you just need to choose from the many provided in the image page on **dockerhub**.

The second instruction I provided is ports: we are telling docker to map the port 80 on our host, to the port 80 on the container: this way will appear as we were running the web server directly on our system.

I used the volumes instruction to specify a bind mount. Since during development the code changes a lot and fast, there would be no sense in putting the code directly inside a container: this way we should rebuild it every time we make some modifications. Instead, what we are going to do is to tell docker to bind-mount the DocumentRoot directory, at /var/www/html inside the container. This directory represents the main **nginx VirtualHost** document root, therefore the code we put inside it, will be immediately available.

The same way I did for bedrock folder in volumes.


**MySQL Database**


We already know what the image keyword is for. The same goes for the volumes instruction,here I created a new volume called dbdata for data persistency ans also for copy the file to the docker instance.

I defined the environment variables under the environment tag, those are used to connect to databse as root user which we will use for database

**Wordpress service**

WordPress is a free and open-source Content Management System (CMS) built on a MySQL database with PHP processing. 

volumes: I mounted a named volume called wordpress to the ./bedrock:/var/www/html mountpoint created by the WordPress image. Using a named volume in this way I allowed us to share our application code with other containers.

depends_on: This option ensures that our containers will start in order of dependency, with the wordpress container starting after the db container. Our WordPress application relies on the existence of our application database and user, so expressing this order of dependency will enable our application to start properly.

networks:I also added the wordpress container to the wpsite-network network.

**PHP admin for the databse view**

The image section allows us to create a container with the official image of PhpMyAdmin. The Container section allows to give a specificc name.

The environment section allows us to define the HOST and password of the MySQL administrator. Be careful to put the same information as in the db container.

The depends_on section allows us to link the container to our database container. Indeed, the phpmyadmin container needs to know the location of the database (in our case, the mysql container).

Finally, the ports section allows us to link our container to our local machine. The first parameter is the local port (here 8081), the second parameter is the port in the container (here 80). We can therefore access PhpMyAdmin by typing the url: http://localhost:8081.

I defined the environment variables under the environment tag, which we will use for database. For the db service, it maps the port 3308 on the host to port 3306 on the MySQL container. For the phpmyadmin service, it maps the port 8080 on the host to port 80 on the phpMyAdmin container.

you can check all the running services with the command

`docker ps`

we can restart the containers if we change anything on configuration or docker-compose file with below command

`docker restart 'container_name'`


Problem : After successfully importing sage theme in wordpress interface with bedrock/web/app/themes/First_theme, I can able to see the new theme in wordpress interface, but I can not able to succeed to apply new theme to my-website page.


I hope my explanation is clear on wordpress with docker, bedrock and sage.



