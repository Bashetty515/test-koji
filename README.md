# Devops technical interview
Hi and thank you for your interest in Koji.  
At Koji, your role as a DevOps will be to : 
- Help and monitor our customer infrastructure on AWS, Google Cloud, Azure (not Azure we're nice people).
- Write Docker compose and Dockerfiles, Ansible playbooks, and many other kinds of fun YAML, HAML, and JSON files.
- Be part of a "DevOps" culture that promotes automation and scalability inside and outside of the company.

## Someone said test?

This test should be done between 1 and 3 hours depending on your abilities and seniority. Take deep breaths there is no trap, no hidden surprise that would force your to re-do the whole test.  
Please read the instruction carefully, failure to respect the expected output would result in the invalidation of your test.  

### 1. How to send your work?
You must open a pull request with a file named INSTRUCTIONS.md containing clear instructions about what you have done and if applicable the software that must be installed before running your work.  

### 2. The exercise
At Koji, we like WordPress and more particularly the WordPress, sage + bedrock + trellis stack.  
If you are not familiar with WordPress, PHP or any other elements of this stack do not worry, it's not a requirement.  
The short explanation is that trellis offers a way to have a local WordPress running on vagrant with a starter theme and a specific setup that can be done in a few minutes thanks to a series of excellent ansible scripts.  

Your goal is to do the same thing with docker and docker-compose.  
Usually when we set up a new project we follow the instructions available here: https://roots.io/docs/trellis/master/installation/#create-a-project.  
Technically it's a quite straightforward process: git clone, composer install, vagrant up, vagrant provision, and it's done.  
Let's do the same thing with docker.  If you can deliver a single file (docker-compose, makefile, shell script ...) that allows to build and start a docker running WordPress with bedrock and sage that would be fantastic!  

### 3. Important note
You can fail, you can do only a part of the job, or deliver something a bit buggy.  
It happens, and we will still consider your work. We want to see how you work, how you approached this challenge.  
Deliver something that runs, and that is documented, and if it's not finished list what is missing and how you would approach the last steps.
